package org.mbari.services.model;

import com.google.protobuf.util.JsonFormat;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mbari.model.Event;
import org.mbari.model.Resource;

import javax.net.ssl.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

/**
 *
 */
@WebServlet(value = "/model-handler", name = "ModelHandlerServlet")
public class ModelHandlerServlet extends HttpServlet {

    // Grab a logger
    private static final Logger LOGGER = Logger.getLogger(ModelHandlerServlet.class.getName());

    // The event constants
    public static final String MODIFY = "MODIFY";
    public static final String MOVED_TO = "MOVED_TO";
    // public static final String MOVED_FROM = "MOVED_FROM";
    // public static final String MOVE = "MOVE";
    public static final String CREATE = "CREATE";
    public static final String CREATE_ISDIR = "CREATE:ISDIR";
    public static final String DELETE = "DELETE";
    public static final String DELETE_ISDIR = "DELETE:ISDIR";

    // Some constants to reflect the known state of a resource
    public static final String RESOURCE_EXISTS = "EXISTS";
    public static final String RESOURCE_DOES_NOT_EXIST = "NOT_EXIST";
    public static final String RESOURCE_AVAILABILITY_UNKNOWN = "UNKNOWN";

    // Resource types
    public static final String RESOURCE_TYPE_FILE = "FILE";
    public static final String RESOURCE_TYPE_DIRECTORY = "DIRECTORY";

    // The RabbitMQ connection properties
    private String rabbitMQHostname = null;
    private Integer rabbitMQPort = null;
    private String rabbitMQVhost = null;
    private String rabbitMQUser = null;
    private String rabbitMQPass = null;
    private String rabbitMQExchangeType = "fanout";
    private String rabbitMQRoutingKey = "*";

    // A boolean to indicate if we are connected to the RabbitMQ server
    private boolean connectedToMessagingServer = false;

    // This is the AMQP channel that will be published to
    private Channel channel;

    // This is a custom TrustManager and SSLContext so we can ignore invalid SSL certificates.  This client only makes
    // HEAD requests so not too worried about invalid SSL certs.
    // TODO kgomes this was done so that I could use with development certificates, there should probably be a way to
    // disable this for production
    private TrustManager[] trustAllCerts = null;
    private SSLContext sslContext = null;

    // A hostname verifier that is needed when the server tries to see if a resource exists on a server. The way this
    // server is used it often times will be looking at hosts inside a Docker container so hosts like 'localhost' or
    // 'httpd' might be used which often times won't match the hostname in the SSL cert.  This allows us to ignore that
    private HostnameVerifier hv = new HostnameVerifier() {
        public boolean verify(String urlHostName, SSLSession session) {
            LOGGER.fine("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
            return true;
        }
    };

    // This is a hashmap that keeps track of the previously received event for a specific repository.  That can help
    // in determining complex changes that happen over multiple events (like a resource moving is actually a delete
    // followed by a modify)
    private HashMap<String, HashMap<String, Object>> repositoriesEventTracker = new HashMap<>();

    /**
     * This is the method that will attempt to connect to the RabbitMQ server and if successful, will set a flag
     * indicating it is connected
     *
     * @return
     */
    private void connectToMessagingServer() {

        // Set the boolean to indicate if we were able to connect
        this.connectedToMessagingServer = false;

        // If the channel exists, close it
        if (channel != null) {
            try {
                channel.close();
            } catch (IOException e) {
                LOGGER.severe("IOException trying to close the RabbitMQ channel before re-opening: " +
                        e.getMessage());
            } catch (TimeoutException e) {
                LOGGER.warning("TimeoutException trying to close the RabbitMQ channel before re-opening: " +
                        e.getMessage());
            }
        }

        // Now set up the AMQP connection factory using the configuration information
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(this.rabbitMQHostname);
        factory.setPort(this.rabbitMQPort);
        LOGGER.info("Connecting to RabbitMQ server at " + rabbitMQHostname + ":" + rabbitMQPort);
        if (this.rabbitMQVhost != null && !this.rabbitMQVhost.equals("")) {
            factory.setVirtualHost(this.rabbitMQVhost);
            LOGGER.info("Set VHost on connection to " + this.rabbitMQVhost);
        }
        if (this.rabbitMQUser != null && !this.rabbitMQUser.equals("")) {
            factory.setUsername(this.rabbitMQUser);
            LOGGER.info("Will connect to RabbitMQ as user " + this.rabbitMQUser);
            if (this.rabbitMQPass != null && !this.rabbitMQPass.equals("")) {
                factory.setPassword(this.rabbitMQPass);
            }
        }

        // Now create the connection
        Connection connection = null;
        try {
            connection = factory.newConnection();
            LOGGER.info("New connection created");
        } catch (TimeoutException e) {
            LOGGER.severe("The connection to the AMQP host " + this.rabbitMQHostname + " timed out: " +
                    e.getMessage());
        } catch (Exception e) {
            LOGGER.severe("Exception caught trying to create new connection to RabbitMQ: " + e.getMessage());
        }

        // Check the connection
        if (connection != null) {

            // Now create the channel
            try {
                this.channel = connection.createChannel();
                LOGGER.info("Channel created");
                this.connectedToMessagingServer = true;
            } catch (IOException e) {
                LOGGER.severe("IOException caught trying to create channel to RabbitMQ server: " + e.getMessage());
            }

        } else {
            LOGGER.severe("No connection was established to the AMQP host");
        }
    }

    /**
     * This method takes in a message and publishes it to a RabbitMQ exchange
     *
     * @param rabbitMQExchangeName
     * @param jsonMessage
     */
    private void sendMessage(String rabbitMQExchangeName, String jsonMessage) {

        // Check for a connection
        while (!this.connectedToMessagingServer) {

            // Try to connect
            this.connectToMessagingServer();

            // If unsuccessful, sleep and it should look again and try
            if (!this.connectedToMessagingServer) {
                LOGGER.severe("Not connected to RabbitMQ, will sleep and try again in 5 seconds");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    LOGGER.severe("Exception caught trying to sleep between connection attempts");
                }
            }
        }

        // Declare the exchange given to ensure that it exists (it will be created if it does not)
        try {
            channel.exchangeDeclare(rabbitMQExchangeName, this.rabbitMQExchangeType);
        } catch (IOException e) {
            LOGGER.severe("Something went wrong trying to declare an exchange named " +
                    rabbitMQExchangeName + ": " + e.getMessage());
            this.connectedToMessagingServer = false;
        }

        // Now publish the message
        try {
            channel.basicPublish(rabbitMQExchangeName, this.rabbitMQRoutingKey,
                    null, jsonMessage.getBytes("UTF-8"));
        } catch (IOException e) {
            LOGGER.severe("Could not publish message: " + e.getMessage());
            this.connectedToMessagingServer = false;
        }
    }

    /**
     * This method takes in a URL (in string form) of a Resource that may or may not exist.  It makes a HTTP HEAD
     * request of the resource just to see if it exists and returns a string indicating the state as best it could
     * figure out.
     *
     * @param resourceUrlString
     * @return
     */
    private String doesResourceExist(String resourceUrlString) {
        LOGGER.fine("Going to check of resource at URL " + resourceUrlString + " exists...");

        // Start off with default unknown
        String resourceState = RESOURCE_AVAILABILITY_UNKNOWN;

        // First let's try to convert the URL string to a URL object
        URL resourceUrl = null;
        try {
            resourceUrl = new URL(resourceUrlString);
        } catch (MalformedURLException e) {
            LOGGER.warning("Could not create a URL object from the URL string " + resourceUrlString + ": " +
                    e.getMessage());
        }

        // Now that we have a URL, create a connection
        HttpURLConnection urlConnection = null;

        // Now try to make the connection to the resource
        if (resourceUrl != null) {
            try {
                urlConnection = (HttpURLConnection) resourceUrl.openConnection();
            } catch (IOException e) {
                LOGGER.warning("Could not open connection to URL " + resourceUrlString + ": " + e.getMessage());
            }
        }
        // Check to see if connection was made
        if (urlConnection != null) {
            try {
                // Set the request to HEAD to only return headers
                urlConnection.setRequestMethod("HEAD");

                // If the URL starts with https, add a custom host verifier to ignore SSL cert host mismatches
                if (resourceUrlString.startsWith("https://")) {
                    ((HttpsURLConnection) urlConnection).setHostnameVerifier(this.hv);
                }
            } catch (ProtocolException e) {
                LOGGER.warning("ProtocolException caught trying to set the request method to HEAD: " +
                        e.getMessage());
            }
        }

        // Now connect to make request for headers
        int responseCode = -1;
        try {
            responseCode = urlConnection.getResponseCode();
        } catch (IOException e) {
            LOGGER.warning("IOException caught trying to get response code from server: " + e.getMessage());
        }
        LOGGER.fine("HEAD request for resource at url " + resourceUrlString +
                " returned response code of " + responseCode);

        // If the response code is in the 200's we will consider that to mean the resource exists
        if (responseCode >= 200 && responseCode < 300) {
            resourceState = RESOURCE_EXISTS;
        } else {
            // So it seems like the resource is not available, but if the response is something other than a 404, log it
            if (responseCode == 404) {
                // This means the resource does not exist for sure
                resourceState = RESOURCE_DOES_NOT_EXIST;
            } else {
                // Not sure about the response code so we will leave the resource state as unknown and log it
                LOGGER.warning("Resource at URL " + resourceUrlString + " returned a response code of " +
                        responseCode + " which is unusual");
            }
        }

        // Now return the answer
        return resourceState;
    }

    /**
     * This method is executed the first time the servlet is called
     */
    @Override
    public void init() {
        LOGGER.info("Model Handler Servlet initializing ... ");

        // Grab the RabbitMQ connection properties from the environment
        // First try to find the hostname
        this.rabbitMQHostname = System.getenv("MODEL_HANDLER_SERVLET_RABBITMQ_HOSTNAME");
        if (this.rabbitMQHostname == null) {
            LOGGER.severe("No RabbitMQ hostname was specified via environment variable " +
                    "MODEL_HANDLER_SERVLET_RABBITMQ_HOSTNAME");
        } else {
            LOGGER.info("RabbitMQ host is " + this.rabbitMQHostname);
        }

        // Now the port
        String rabbitMQportString = System.getenv("MODEL_HANDLER_SERVLET_RABBITMQ_PORT");
        if (rabbitMQportString != null) {
            try {
                this.rabbitMQPort = Integer.parseInt(rabbitMQportString);
                LOGGER.info("RabbitMQ port set to " + this.rabbitMQPort);
            } catch (NumberFormatException e) {
                LOGGER.severe("Could not convert the environment variable " +
                        "MODEL_HANDLER_SERVLET_RABBITMQ_PORT to an integer, will use 5672 by default");
                this.rabbitMQPort = 5672;
            }
        } else {
            LOGGER.warning("No RabbitMQ port was specified via the environment " +
                    "variable MODEL_HANDLER_SERVLET_RABBITMQ_PORT will use 5672 by default");
            this.rabbitMQPort = 5672;
        }

        // Now the default VHost
        this.rabbitMQVhost = System.getenv("MODEL_HANDLER_SERVLET_RABBITMQ_VHOST");
        if (this.rabbitMQVhost == null) {
            LOGGER.severe("No RabbitMQ VHost was specified via environment variable " +
                    "MODEL_HANDLER_SERVLET_RABBITMQ_VHOST");
        } else {
            LOGGER.info("RabbitMQ VHost is " + this.rabbitMQVhost);
        }

        // Now the username that will be used to connect to the RabbitMQ server
        this.rabbitMQUser = System.getenv("MODEL_HANDLER_SERVLET_RABBITMQ_USER");
        if (this.rabbitMQUser == null) {
            LOGGER.severe("No RabbitMQ user was specified via environment variable " +
                    "MODEL_HANDLER_SERVLET_RABBITMQ_USER");
        } else {
            LOGGER.info("RabbitMQ user is " + this.rabbitMQUser);
        }

        // And the password
        this.rabbitMQPass = System.getenv("MODEL_HANDLER_SERVLET_RABBITMQ_PASS");
        if (this.rabbitMQPass == null) {
            LOGGER.severe("No RabbitMQ password was specified via environment variable " +
                    "MODEL_HANDLER_SERVLET_RABBITMQ_PASS");
        } else {
            LOGGER.info("RabbitMQ password was found in the environment");
        }

        // Try to connect to the RabbitMQ server
        this.connectToMessagingServer();
    }

    /**
     * @param jsonObject
     * @param code
     * @param message
     * @param domain
     * @param reason
     * @param detailedMessage
     */
    private void setErrorMessage(JSONObject jsonObject, Integer code, String message, String domain, String reason,
                                 String detailedMessage) {
        // Set the code to 400 in that the request was not valid
        jsonObject.append("code", code);

        // And the short message
        jsonObject.append("message", message);

        // Create an error object to hold the details
        JSONObject errorObject = new JSONObject("{}");

        // And set the details
        errorObject.append("domain", domain);
        errorObject.append("reason", reason);
        errorObject.append("message", detailedMessage);

        // Add the error details
        JSONArray errorArray = new JSONArray();
        errorArray.put(errorObject);
        jsonObject.append("errors", errorArray);

    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // This really shouldn't be supported, but we will just forward to the doPost handler
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Set the response format to JSON
        response.setContentType("application/json");

        // Grab the response writer
        PrintWriter responseWriter = response.getWriter();

        // Let's start a JSON Object that will be used to format the reply
        JSONObject replyObject = new JSONObject("{}");

        // We first need to read in the message from the request
        StringBuilder messageBuilder = new StringBuilder();
        BufferedReader br = request.getReader();
        String str;
        while ((str = br.readLine()) != null) {
            messageBuilder.append(str);
        }
        LOGGER.fine("Incoming message is: " + messageBuilder.toString());

        // Try to convert it to a JSON Object
        JSONObject messageJSON = null;
        try {
            messageJSON = new JSONObject(messageBuilder.toString());
        } catch (JSONException e) {
            LOGGER.warning("Could not convert message body to JSON: " + e.getMessage());
            LOGGER.warning("Body read was:\n" + messageBuilder.toString());
        }

        // Make sure we converted to an object OK
        if (messageJSON != null) {

            // Grab the parameters that might be there
            Long timestamp = null;
            String timestampString = messageJSON.getString("timestamp");
            // Sometimes the timestamp starts with an '=' if it's coming from fswatch, so just remove that
            try {
                if (timestampString.startsWith("=")) {
                    timestamp = Long.parseLong(timestampString.substring(1));
                } else {
                    timestamp = Long.parseLong(timestampString);
                }
            } catch (NumberFormatException e) {
                LOGGER.warning("Could not convert timestamp of " + timestampString + " to a long");
            }
            String eventList = messageJSON.getString("event");
            String repoName = messageJSON.getString("repoName");
            String resourceURL = messageJSON.getString("resourceURL");
            String resourceType = messageJSON.getString("resourceType");

            // First record the event in the tracker hashmap
            HashMap<String, Object> repositoryEventTracker = repositoriesEventTracker.get(repoName);

            // If one is not found, create and add it to the hashmap of trackers
            if (repositoryEventTracker == null) {
                repositoryEventTracker = new HashMap<>();
                repositoriesEventTracker.put(repoName, repositoryEventTracker);
            }
            // Now grab the previous entries (if any)
            Long previousTimestamp = (Long) repositoryEventTracker.get("timestamp");
            String previousEventList = (String) repositoryEventTracker.get("eventList");
            String previousResourceURL = (String) repositoryEventTracker.get("resourceURL");

            // Now make sure we got what we need to construct a valid message
            if (eventList == null || repoName == null || resourceURL == null || resourceType == null) {
                // Not all the parameters were there so reply with an error
                this.setErrorMessage(replyObject, 400, "Bad Request", "mbari.org",
                        "JSON message incomplete", "The body of the request should contain " +
                                "JSON that has the following properties: timestamp, event, repoName, resourceURL, " +
                                "and resourceType and one or more of those are missing");
            } else {
                // OK, looks like we are good to keep going so let's log what we got in the request
                // TODO kgomes I should validate that eventList is understood
                LOGGER.fine("Message received");
                LOGGER.fine("Timestamp: " + timestamp);
                LOGGER.fine("Events: " + eventList);
                LOGGER.fine("Repo name: " + repoName);
                LOGGER.fine("Resource URL: " + resourceURL);
                LOGGER.fine("Resource Type: " + resourceType);

                // Create an event builder
                Event.Builder eventBuilder = Event.newBuilder();

                // Add the timestamp
                eventBuilder.setTimestamp(timestamp);

                // Add the event string
                eventBuilder.setSourceMessage(eventList);

                // First, let's check to see if the resource at the give locator exists
                String resourceState = doesResourceExist(resourceURL);

                // Now look for what type of event this is
                if (eventList.equalsIgnoreCase(CREATE) || eventList.equalsIgnoreCase(CREATE_ISDIR)) {
                    // TODO Verify file exists at locator
                    eventBuilder.setType(CREATE);
                } else if (eventList.equalsIgnoreCase(MODIFY)) {
                    // First, just set it to MODIFY
                    eventBuilder.setType(MODIFY);

                    // Modify can be a bit of a funny one, it can truly be a modify or it can be the end of a move
                    // In order to figure out if it's simple move (not rename), we can apply the following logic:
                    // 1. The previous event for this repository was a DELETE
                    // 2. The previous timestamp is the same as the current one
                    // 3. The previous resource URL ends with the same URL portion as this one
                    if ((previousEventList != null) && (previousTimestamp != null) &&
                            (previousEventList.equalsIgnoreCase(DELETE) ||
                                    previousEventList.equalsIgnoreCase(DELETE_ISDIR)) &&
                            (previousTimestamp == timestamp)) {
                        // Now check the URL portions
                        if (previousResourceURL != null && resourceURL != null) {
                            String[] previousResourceURLParts = previousResourceURL.split("/");
                            String[] resourceURLParts = resourceURL.split("/");
                            if (previousResourceURLParts[previousResourceURLParts.length - 1]
                                    .equals(resourceURLParts[resourceURLParts.length - 1])) {
                                // This is likely the end of a move
                                eventBuilder.setType(MOVED_TO);
                            }
                        }
                    }
                } else if (eventList.equalsIgnoreCase(DELETE)) {
                    // Updated event
                    eventBuilder.setType(DELETE);
                } else if (eventList.equalsIgnoreCase(DELETE_ISDIR)) {
                    // Updated event
                    eventBuilder.setType(DELETE);
                } else {
                    LOGGER.warning("Did not understand the event from the following line:\n " +
                            messageBuilder.toString());
                }

                // Now let's create the equivalent resource and attach it to the event
                Resource.Builder resourceBuilder = Resource.newBuilder();

                // Try to see if it is a file or directory (if it exists locally)
                if (resourceType.equals(RESOURCE_TYPE_FILE)) {
                    resourceBuilder.addTypes(RESOURCE_TYPE_FILE);
                } else {
                    resourceBuilder.addTypes(RESOURCE_TYPE_DIRECTORY);
                }

                // Add the URL as a locator
                resourceBuilder.addLocators(resourceURL);

                // Let's build the resource
                Resource resource = resourceBuilder.build();

                // Now add it to the event builder
                eventBuilder.addResources(resource);

                // Now build the event
                Event event = eventBuilder.build();

                // If there is a Event, send it
                String eventJson = JsonFormat.printer().print(event);
                LOGGER.fine("Sending the following message to repo " + repoName + ":\n" + eventJson);
                this.sendMessage(repoName, eventJson);

                // And now set response message
                replyObject.append("code", 200);
                replyObject.append("message", "Following message sent to RabbitMQ:\n " + eventJson);

                // Now update the repository event tracker
                repositoryEventTracker.put("timestamp", timestamp);
                repositoryEventTracker.put("eventList", eventList);
                repositoryEventTracker.put("resourceURL", resourceURL);
            }
        } else {
            // Since there was an exception, go ahead and set the reply to an error
            this.setErrorMessage(replyObject, 400, "Bad Request", "mbari.org",
                    "No valid JSON in body",
                    "This servlet is looking for JSON in the body of the POST request and the " +
                            "body that was read from your request could not be converted to JSON.  The body that was " +
                            "read from the request is\n" + messageBuilder.toString());
        }

        // Now send the reply
        responseWriter.println(replyObject.toString());
    }
}

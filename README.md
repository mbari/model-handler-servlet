# model-handler-servlet

This project contains a servlet that listens for messages about objects that are MBARI Model objects and formats the messages and forwards them to a RabbitMQ server.

This project is related to the data modeling project located here:

https://bitbucket.org/mbari/mbari-data-model

You don't need to checkout the above project because the latest version of the code should be deployed in the Bintray Maven repo (see the pom.xml/settings.xml file in this project for details).

There are two ways you can use the code in this repository.  You can simply build the WAR file and deploy it to a Tomcat server somwhere, or you can build an image using the Dockerfile included in this project. In order to build the WAR file, you simply use:

```bash
mvn package
```

which will create a model-handler.war file in a target directory.  You can then deploy this war file in Tomcat container.  In order for this servlet to work properly, you need to run the Tomcat container with some environment variables so that the servlet will know where to publish messages to.  The variables are:
1. MODEL_HANDLER_SERVLET_RABBITMQ_HOSTNAME: This is the hostname where the RabbitMQ server can be found.
1. MODEL_HANDLER_SERVLET_RABBITMQ_PORT: This is the port where RabbitMQ is listening for connections.
1. MODEL_HANDLER_SERVLET_RABBITMQ_VHOST: This is the name of the VHost on the RabbitMQ server that this servlet will connect to.
1. MODEL_HANDLER_SERVLET_RABBITMQ_USER: This is the username that will be used to connect to the RabbitMQ server
1. MODEL_HANDLER_SERVLET_RABBITMQ_PASS: This is the password that will be used to connect to the RabbitMQ server

To run this inside Docker, you simply run something like:
```bash
docker build -t model-handler-servlet-image .
```

which will build a Docker image and tag it with the name model-handler-servlet.  You can then create an environment file (e.g. 'env') that contains the environment variables listed above and run the Docker image.  An example 'env' file might look like:

```bash
MODEL_HANDLER_SERVLET_RABBITMQ_HOSTNAME=localhost
MODEL_HANDLER_SERVLET_RABBITMQ_PORT=5672
MODEL_HANDLER_SERVLET_RABBITMQ_VHOST=my-vhost
MODEL_HANDLER_SERVLET_RABBITMQ_USER=my-rabbit-username
MODEL_HANDLER_SERVLET_RABBITMQ_PASS=my-rabbit-password
```
Then you can start the Docker image using something like:
```bash
docker run -p 8080:8080 --env-file ./env -v /my/host/logs/directory:/usr/local/tomcat/logs --name model-handler-servlet model-handler-servlet-image
```

The -v argument mounts a directory on your host machine to the location of the Tomcat server inside the container so you can have easy visibility to the log files.

## Message format
Currently, this servlet does not do a whole lot, but is more of a re-packager as it takes an incoming JSON message and then publishes it to RabbitMQ.  The incoming message format is of the form:
```json
{
  "timestamp":"1535494904",
  "event":"CREATE",
  "repoName":"activities-activity-1",
  "resourceUrl":"http://my-host.com/data/repo/to/monitor/some/directory/and/a/file.txt",
  "resourceType":"FILE"
}
```
The servlet will parse this JSON, format a message and send it to the ```activities-activity-1``` exchange on RabbitMQ as:
```json
 {
   "type": "CREATE",
   "sourceMessage": "CREATE", 
   "timestamp": "1535494904",
   "resources": [
     {
       "types": [
         "FILE"
       ],
       "locators": [
         "http://my-host.com/data/repo/to/monitor/some/directory/and/a/file.txt"
       ]
     }
   ]
 }
```

The expected message format comes, in large part, from a project that uses inotify to push file system changes to an HTTP URL.  That project can be found here: https://bitbucket.org/mbari/inotify-post

## Logic

As mentioned at the end of the last section, the way that this servlet handles the incoming messages depends largely on what it expects from the inotify-post project.  Based on that project, certain assumptions had to be made in order to process the messages coming from an inotify client.  Here are the rules that are followed when processing incoming messages:

1. When a message of CREATE is received, it is assumed that this is a straight up creation of a resource (either a file or a directory).  A message will simply be created to reflect the creation of the new resource.
1. 